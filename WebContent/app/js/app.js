'use strict';

angular.module('weatherWidgetApp',['ngRoute','weatherWidget.services','weatherWidget.directives','weatherWidget.controllers']).
	config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/weatherWidget', {templateUrl: 'partials/weatherWidget.html', controller: 'weatherWidgetCtrl'});
		$routeProvider.otherwise({redirectTo: '/weatherWidget'});
	}
]);