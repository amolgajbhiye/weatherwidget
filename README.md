# README #

This README is documented for illustrating the steps required to get Weather Widget application up and running.

### What is this repository for? ###

* This repository contains source code of a small angular JS application. 
* This application shows weather forecast of our city by using the API [OpenWeatherMap](http://openweathermap.org/)

### Steps to run application ###

> 1. Download some pre-requisite softwares
>> 1. Node JS for Windows
>> 2. Run Jetty from eclipse Marketplace
>> 3. Sourcetree
>> 4. Eclipse

> 2. Install all pre-requisite softwares in your system.
> 3. Open [BitBucket](https://bitbucket.org/amolgajbhiye/weatherwidget) and click on clone icon. On clicking it, click on clone in sourcetree.
> 4. Once you click on "Clone in SourceTree" automatically sourcetree will be opened and start cloning the repository.
> 5. Once cloning process gets completed, click on Repository --> Repository Settings --> Advanced and remember the origin path.
> 6. Now open eclipse and navigate through File --> Import --> General --> Existing Project in Workspace and select project from the origin path.
> 7. After importing, Convert project into Maven project by navigating  through Right click on project --> Maven --> Update Project --> Check on Force Update --> Click Ok.
> 8. After converting project into Maven Project, go to the origin path of project in file explorer and open command prompt on that location. Use command Shift + Right mouse key and select "Run Command Prompt".
> 9. On command Prompt Run following commands
>> 1. npm install
>> 2. bower install
> 10. Come back to eclipse right click on project and naviagate through Run As --> Run Configurations --> Jetty WebApp --> change port Number as 8080 and context as /WeatherWidget. --> Ok
> 11. After setting context path navigate through Run As --> Maven Install.
> 12. After completion of Maven install, start the Jetty Server by navigating through Right click on project --> Run As --> Run Jetty.
> 13. Once server will [click here](http://localhost:8080/WeatherWidget/app) to run the application.

### Note : Ignore steps 7-9 because I have committed all required library files in repository.