'use strict';

angular.module('weatherWidget.controllers',[]).controller('weatherWidgetCtrl',['$scope','weatherWidgetFactory',function($scope,weatherWidgetFactory){
    $scope.forecast = weatherWidgetFactory.weatherDetails({
      location: 'Pune'
    });
  }])
