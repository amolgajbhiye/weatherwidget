'use strict';

angular.module('weatherWidget.directives', [])
  .directive('weatherPanel',[function factory() {
    return {
      scope: {
        useDayForecast: '=showEntry',
      },

      templateUrl: 'partials/weatherTemplate.html',      

      link: function(scope, element, attrs) {
        scope.getIconImageUrl = function(iconName) {
          return (iconName ? 'http://openweathermap.org/img/w/' + iconName + '.png' : '');
        };

        scope.parseDate = function (time) {
          return new Date(time * 1000);
        };
      }
    }
  }]);